#! /usr/bin/make -f
TARGETS=$(DEPS) $(PROGRAMS)
PROGRAMS=no_volatile
HEADERS=body.h
no_volatile_SRCS=main.c no_volatile.c
no_volatile_OBJS=$(no_volatile_SRCS:.c=.o)
SRCS=$(no_volatile_SRCS)
OBJS=$(no_volatile_OBJS)
DEPS=.depend
CFLAGS=-Wall -g
LDLIBS=-lpthread -lbsd
.PHONY: all depend clean
all: $(TARGETS)
-include $(DEPS)
no_volatile: $(no_volatile_OBJS)
	$(CC) -o $@ $(LDFLAGS) $(no_volatile_OBJS) $(LDLIBS)
depend: $(DEPS)
$(DEPS): $(SRCS) $(HEADERS)
	$(CC) -M -o $(DEPS) $(SRCS)
clean:
	$(RM) $(TARGETS) $(OBJS) core core.* core-*
