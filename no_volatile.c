/**
 * @file body.c
 * @author Mitsutoshi Nakano <ItSANgo@gmail.com>
 * @brief 
 * @version 0.1.0
 * @date 2020-07-04
 * 
 * Copyright (c) 2020 Mitsutoshi Nakano <ItSANgo@gmail.com>
 * 
 */

#include <stdio.h>
#include <limits.h>
#include <stdlib.h>
#include <signal.h>
#include <pthread.h>
#include <sysexits.h>
#include <bsd/err.h>
#include "body.h"

enum {
	THREADS = 250
};

struct th_info {
	pthread_t th;
	int create_status;
	int arg;
} info[THREADS];

void
init_th_info(void)
{
	int i;
	for (i = 0; i < THREADS; ++i) {
		info[i].create_status = -1;
		info[i].arg = i;
	}
}

void *
exit_soon_routine(void *argp)
{
	int *ret = malloc(sizeof(int));
	if (ret == NULL) {
		warn("malloc()");
	} else {
		int *intp = argp;
		int arg = *intp;
		*ret = arg + 1;
	}
	pthread_exit(ret);
}

void
create_a_thread(struct th_info *p, int arg)
{
	p->arg = arg;
	p->create_status = pthread_create(&p->th, NULL,
		exit_soon_routine, &p->arg);
}

void
create_all(void)
{
	int i;
	for (i = 0; i < THREADS; ++i) {
		struct th_info *p = info + i;
		create_a_thread(p, i);
	}
}

void
join_a_thread(pthread_t th)
{
	void *ret;
	int join_err = pthread_join(th, &ret);
	if (join_err) {
		warnc(join_err, "pthraed_join(%lx, %p)",
			th, (void *)&ret);
		return;
	}
	if (ret == NULL) {
		return;
	}
	int *valp = ret;
	printf("%d\n", *valp);
	free(ret);
}

void
join_all(void)
{
	int i;
	for (i = 0; i < THREADS; ++i) {
		if (info[i].create_status == 0) {
			join_a_thread(info[i].th);
		}
	}
}

int
body(int argc, char *argv[])
{
	init_th_info();
	create_all();
	join_all();
	return EX_OK;
}
